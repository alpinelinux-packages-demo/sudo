# [sudo](https://pkgs.alpinelinux.org/packages?name=sudo)

Give certain users the ability to run some commands as root

(Unofficial demo and howto)

## Contents
* Shows some package configuration

## See also
* [gitlab.com/hub-docker-com-demo/alpine
  ](https://gitlab.com/hub-docker-com-demo/alpine)
  * For examples using "sudo"